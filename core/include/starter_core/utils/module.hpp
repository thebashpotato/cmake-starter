#ifndef STARTER_CORE_UTILS_MODULE_HPP
#define STARTER_CORE_UTILS_MODULE_HPP

/**
 * @file starter_core/utils/module.hpp
 * @author Matt Williams (matt.k.williams@protonmail.com)
 * @brief Module/Barrel file for utils
 * @date 2023-02-26
 */

#include <starter_core/utils/version.hpp>

#endif
