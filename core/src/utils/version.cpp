#include <starter_core/utils/version.hpp>

namespace starter::core {
std::string get_version() {
  return std::string{VMAJOR}.append(".").append(VMINOR).append(".").append(
      VPATCH);
}
} // namespace starter::core
