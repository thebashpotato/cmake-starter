#include <gtest/gtest.h>
#include <starter_core/utils/module.hpp>

/**
 * @brief Test the utils module
 *
 * */
TEST(CoreLibraryVersionTest, BasicAssertions) {
  // Expect equality.
  EXPECT_EQ("0.1.0", starter::core::get_version());
}
