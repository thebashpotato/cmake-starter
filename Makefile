# Author: Matt Williams <matt.k.williams@protonmail.com>

# Change to the name of your software
APP_NAME:=starter
# Update version to match root CMakeLists.txt version
APP_VERSION:=0.1.0
DOCKER_TAG:=${APP_NAME}:v${APP_VERSION}
SHELL:=$(shell command -v sh)
DEVKIT:=python3 scripts/devkit.py

define _list =
	cat << EOF
		==============================
		| Command   ||   Description |
		==============================
		list        --   Lists all available commands
		init        --   Initializes and installs all tools and packages (only ran once, relies on pacman)
		docs        --   Builds doxygen documentation
		container   --   Builds a custom arch linux docker container image
		release     --   Builds optimized binary
		develop     --   Builds debugging binary
		run         --   Builds debug binary and runs it
		format      --   Recursively runs clang-format all legal cmake and source files
		tidy        --   Runs clang-tidy on on all legal source files
		tests       --   Runs ctest on the test suite
		install     --   Installs the optimized binary
		uninstall   --   Uninstalls the optimized binary
		clean       --   Cleans all build artifacts
	EOF
endef

define _init =
	if command -v "pacman" 1>/dev/null 2>&1; then
		echo "Installing basic tools for arch linux base distro.."
		echo
		sudo pacman -Syu --needed --noconfirm base-devel cmake dialog ninja clang python-pip doxygen

		if [ "$$USER" != "root" ]; then
			pip install distro clang-format --user
		fi

	else
		echo "Unsupported package manager detected, cannot install packages..."
		echo "Feel free to submit a pull request to add support for your distros package manager"
	fi
endef

define _docs =
	echo "Building docs"
	[ -d ./doxygen/html ] && rm -rf ./doxygen/html
	doxygen ./doxygen/Doxyfile
endef

define _container =
	if command -v "docker" 1>/dev/null 2>&1; then
		docker image build --tag ${DOCKER_TAG} -f dev_containers/ArchLinux.Dockerfile .
	else
		echo "Could not build container, docker was not found on the system.."
	fi
endef

define _uninstall =
	if [ ! -d build ] || [ ! -f build/install_manifest.txt ]; then
		echo "Cannot uninstall, no build folder found or no install_manifest.txt file found"
		echo "Try running => make install"
		exit 1
	else
		# xargs is linux only.
		echo "Removing installed files.."
		cat build/install_manifest.txt
		echo
		sudo xargs rm < build/install_manifest.txt
	fi
endef

define _clean =
	echo "Cleaning build artifacts.."
	[ -d build ] && rm -rf build
	[ -d build-debug ] && rm -rf build-debug
	[ -d build-debug-remote ] && rm -rf build-debug-remote
	[ -d cmake-build-debug ] && rm -rf cmake-build-debug
	[ -d cmake-build-debug-remote ] && rm -rf cmake-build-debug-remote
	echo
endef

list:
	@$(call _list)

init:
	@$(call _init)

docs:
	@$(call _docs)

container:
	@$(call _container)

release: clean
	@${DEVKIT} build --release

develop: clean
	@${DEVKIT} build --develop

run: develop
	@./build/app/src/${APP_NAME}

format:
	@${DEVKIT} clang --format

tidy:
	@${DEVKIT} clang --tidy

tests: develop
	@cd build/app && ctest
	@cd .. && cd core/ && ctest

install: release
	@sudo cmake --install build
	@sudo ldconfig

uninstall:
	@$(call _uninstall)

clean:
	@$(call _clean)

.ONESHELL:
.Phony: list init docs container release develop run format tidy tests install uninstall clean
