<div align="center">
  <img width="1000" height="400" src="assets/cmake-starter.jpg">
</div>
<div align="center">
  <img src="https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square">
  <br>
  <p>A <b>cmake starter</b> template for modern C++ development</p>
</div>

## Table of Contents

- [Background](#background)
- [Template](#template)
- [Development](#development)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

A full featured CMake project template for **Linux** C++ programmers. It uses the light weight **CPM** package manager,
**doxygen** for documentation, **docker** for containerization, **clang tooling** files for formating and and static analyzing, custom python modules for
common developer tasks, all wrapped up with a **Makefile** for easy command execution for developing, testing, container builds, or installation.
This project treats **Arch Linux** and its derivatives as a first class citizen, other distros such as Ubuntu and Fedora will added.

1. [CPM -- The missing package manager for CMake](https://github.com/cpm-cmake/CPM.cmake)

- CPM was chosen because it is simple to set up, dependency free, and is a simple wrapper around
  Cmakes FetchContent. Most other package managers like Hunter or Conan are difficult to set up,
  make ci/cd pipelines even more difficult to set up than they already are, and have dependencies.

2. This repository has 2 types of projects ready to go by default.

- The `core` folder showcases how to set up a shared library in CMake
- The `app` folder showcases a standard binary application which consumes the `core` shared library
- If you just want to build a library, delete the `app` folder, and purge the CMakeLists.txt files of `app` specific code
- If you just want to build a regular ol' binary application, delete the `core` folder, and purge the CMakeLists.txt files or `core` specific code

## Template

The following files/directories should be changed/renamed. As a general rule of thumb,
you should change anything that says `starter` or `STARTER` to `YOUR_SOFTWARES_NAME`.

1. CMakeLists.txt (project name, version and description etc)
2. app/CMakeLists.txt (option variable names)
3. core/CMakeLists.txt (option variable names)

- (e.g) change `STARTER_APP_BUILD_TESTING` to `NAME_OF_YOUR_SOFTWARE_APP_BUILD_TESTING`

3. Makefile (`APP_NAME` variable and `APP_VERSION` variable values to your liking)
4. app/include/starter_app (rename `starter_app` directory to `your-softwares-name`)
5. app/include/<your-softwares-name>/utils/version.hpp (change namespace and header gaurd names)
6. app/include/<your-softwares-name>/utils/module.hpp (change namespace and header gaurd names)
7. app/src/utils/version.cpp (Change namespace and include directive names)
8. app/examples/scratch.cpp (Change namespace and include directive names)
9. app/test/app_utils_module_tests.cpp (Change namespace and include directive names)
10. Rinse and Repeat for the core/ directory, it is set up identical.
11. scripts/settings.py **Important** used for project configuration, **PLEASE READ THIS FILE**!!!

## Development

The root Makefile wraps a tool called `devkit.py` which is a collection of python modules that perform common tasks
for developers and package maintainers. It has one pip dependency which is **distro** (it is handled in the init command of the Makefile).
Python version >= 3.6 should be fine. The root Makefile is your friend, _read it, study it, know it well_.

```bash
# Run for all Makefile command options and their descriptions
make list

# Install all development librararies/dependencies (should only be ran once)
make init

# Compiles in debug mode, with tests and examples enabled
make develop

# Compile in release mode, tests and examples are disabled
make release

# Builds the documenation with Doxygen
make docs

# Uses clang-format
make format

# Uses clang-tidy to statically analyze all files.
make tidy

# Install the project in release mode
make install

# Uninstalls using the build/install_manifest.txt file
make uninstall
```

### Docker

1. ArchLinux.Dockerfile builds and runs all tests in the project, simulates a ci/cd pipeline.

```bash
# Makefile command will build and run the container (assuming you have docker installed and configured properly)
make container

# Run the container interactively
docker container run -it starter:v0.1
```

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[AGPLV3](LICENSE) © 2023 Matt Williams
