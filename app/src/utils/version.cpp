#include <starter_app/utils/version.hpp>

namespace starter::app {
std::string get_version() {
  return std::string{VMAJOR}.append(".").append(VMINOR).append(".").append(
      VPATCH);
}
} // namespace starter::app
