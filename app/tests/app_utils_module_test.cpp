#include <gtest/gtest.h>
#include <starter_app/utils/module.hpp>

using namespace starter;

TEST(Version, StarterAppBasicAssertion) {
  EXPECT_EQ("0.1.0", app::get_version());
}
