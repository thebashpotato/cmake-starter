#ifndef STARTER_APP_UTILS_MODULE_HPP
#define STARTER_APP_UTILS_MODULE_HPP

/**
 * @file starter_app/utils/module.hpp
 * @author Matt Williams (matt.k.williams@protonmail.com)
 * @brief Module/Barrel file for utils module
 * @date 2023-02-26
 */

#include <starter_app/utils/version.hpp>

#endif
