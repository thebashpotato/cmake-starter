#ifndef STARTER_APP_UTILS_VERSION_HPP
#define STARTER_APP_UTILS_VERSION_HPP

/**
 * @file starter_app/utils/version.hpp
 * @author Matt Williams (matt.k.williams@protonmail.com)
 * @brief Adds version support for project, used by Cmake
 * @date 2023-02-26
 */

#include <string>

namespace starter::app {
constexpr auto VMAJOR = "0";
constexpr auto VMINOR = "1";
constexpr auto VPATCH = "0";
/**
 * @brief return the version in concatenated format
 *
 * @returns std::string
 * */
[[maybe_unused]] std::string get_version();
} // namespace starter::app

#endif
