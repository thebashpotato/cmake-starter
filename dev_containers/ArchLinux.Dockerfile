FROM archlinux:latest
MAINTAINER Matt Williams <matt.k.williams@protonmail.com>
ENV app_name=starter

RUN pacman -Syu --needed --noconfirm base-devel git cmake dialog ninja clang python-pip doxygen
RUN pip install distro pyaml cmake-format


# Copy all relevant files
COPY ./cmake /$app_name/cmake
COPY ./app /$app_name/app
COPY ./core /$app_name/core
COPY ./CMakeLists.txt /$app_name/CMakeLists.txt
COPY ./Makefile /$app_name/Makefile
COPY ./.clang-format.yaml /$app_name
COPY ./.clang-tidy.yaml /$app_name
COPY ./.cmake-format.yaml /$app_name
COPY ./scripts /$app_name/scripts

WORKDIR /$app_name

RUN make tests
RUN make format
RUN make tidy
